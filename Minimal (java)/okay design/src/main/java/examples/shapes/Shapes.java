package examples.shapes;
/**
 * Shapes
 *
 * This class represents abstract parent class
 */
public abstract class Shapes{
	
	 /**
     * abstract class for Area of the shapes
     * @return                  Area of the shapes
     */
	
	public abstract double computeArea();
	
	/**
     * abstract class to Move the point
     *
     * @param deltaX            The delta amount to move the point in the x direction -- must be a valid double
     * @param deltaY            The delta amount to move the point in the y direction -- must be a valid double
     * @throws ShapeException   Exception throw if any parameter is invalid
     */
	
	public abstract void move(double deltaX ,double deltaY)throws ShapeException;

}
