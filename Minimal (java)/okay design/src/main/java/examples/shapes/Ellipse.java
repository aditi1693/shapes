/**
 * 
 */
package examples.shapes;

/**
 * @author aditi
 *
 */
public class Ellipse extends Shapes {
	
	
	private Point center;
    private double a;
    private double b;
    
    /**
     * Constructor with x-y Location for center
     *
     * @param x                 The x-location of the center of the circle -- must be a valid double
     * @param y                 The y-location of the center of the circle
     * @param radius            The radius of the circle -- must be greater or equal to zero.
     * @throws ShapeException   The exception thrown if the x, y, or z are not valid
     */
    public Ellipse(double x, double y, double a, double b) throws ShapeException {
        Validator.validatePositiveDouble(a, "Invalid Majore-axis Length");
        Validator.validatePositiveDouble(b, "Invalid Minor-axis Length");

        center = new Point(x, y);
        this.a = a;
        this.b = b;
    }

    /**
     * Constructor with a Point for center
     *
     * @param center            The x-location of the center of the circle -- must be a valid point
     * @param radius            The radius of the circle -- must be greater or equal to zero.
     * @throws ShapeException   The exception thrown if the x, y, or z are not valid
     */
    public Ellipse(Point center, double a, double b) throws ShapeException {
        Validator.validatePositiveDouble(a, "Invalid Major Radius");
        Validator.validatePositiveDouble(b, "Invalid Minor Radius");
        
        if (center==null)
            throw new ShapeException("Invalid center point");

        this.center = center;
        this.a = a;
        this.b = b;
    }

    /**
     * @return  The center of the circle
     */
    public Point getCenter() { return center; }

    /**
     * @return  The radius of the circle
     */
    public double getMajorRadius() { return a; }
    
    public double getMinorRadius() { return b; }
    
    public Point getFoci1()throws ShapeException
    {	
      double c,x1;      
      Point foci1;
      c = Math.sqrt((Math.pow(a, 2))-(Math.pow(b, 2)));
      x1 = center.getX() + c;
      foci1 = new Point(x1, center.getY());
      return foci1;
    	
    }
    
    public Point getFoci2()throws ShapeException
    {	
      double c,x1;      
      Point foci1;
      c = Math.sqrt((Math.pow(a, 2))-(Math.pow(b, 2)));
      x1 = center.getX() - c;
      foci1 = new Point(x1, center.getY());
      return foci1;
    	
    }


    /**
     * Move the circle
     * @param deltaX            a delta change for the x-location of center of the circle
     * @param deltaY            a delta change for the y-location of center of the circle
     * @throws ShapeException   Exception thrown if either the delta x or y are not valid doubles
     */
    public void move(double deltaX, double deltaY) throws ShapeException {
        center.move(deltaX, deltaY);
    }

    /**
     * Scale the circle
     *
     * @param scaleFactor       a non-negative double that represents the percentage to scale the circle.
     *                          0>= and <1 to shrink.
     *                          >1 to grow.
     * @throws ShapeException   Exception thrown if the scale factor is not valid
     */
    public void scale(double scaleFactor) throws ShapeException {
        Validator.validatePositiveDouble(scaleFactor, "Invalid scale factor");
        a = a*scaleFactor;
        b = b*scaleFactor;
    }

    /**
     * @return  The area of the circle.
     */
    public double computeArea() {
        return Math.PI * a * b;
    }

}
