/**
 * 
 */
package examples.shapes;

/**
 * @author aditi
 *
 */
public class Triangle extends Shapes{
	

    private Point point1;
    private Point point2;
    private Point point3;
   
	private Line line1;
	private Line line2;
	private Line line3;
	
	private double a,b,c;
	
	  /**
     * Constructor based on X-Y location
     * @param point1            point of first vertices -- must not be null
     * @param point2            point of second vertices -- must not be null
     * @param point3            point of third vertices -- must not be null
     * @throws ShapeException   Exception throw if any parameter is invalid
     * @throws ShapeException   Exception throw if length of any edge is smaller than 0    
     * @throws ShapeException   Exception throw if All vertices are in the same line
     * @throws ShapeException   Exception throw if Sum of two side of triangle is not greater than third side*/

	
	
	  public Triangle(double x1, double y1, double x2, double y2,double x3, double y3) throws ShapeException {
		  

		    
		    point1= new Point(x1,y1);
		    point2= new Point(x2,y2);
		    point3= new Point(x3,y3);
		  
		    line1 = new Line(point1, point2);
		    line2 = new Line(point2, point3);
		    line3 = new Line(point3, point1);
		    
		    a=line1.computeLength();
		    b=line2.computeLength();
		    c=line3.computeLength();

		    
	        if ((y1 - y2) * (x1 - x3) == (y1 - y3) * (x1 - x2))
	        	throw new ShapeException("All vetices should not be in the same line");
	        
	        if (a + b <= c && b + c <= a && c + a <= b)
	        	throw new ShapeException("Sum of two side of triangle must be greater than third side");
	    }
	  
		
	  /**
     * Constructor based on points of Vertices
     * @param point1            point of first vertices -- must not be null
     * @param point2            point of second vertices -- must not be null
     * @param point3            point of third vertices -- must not be null
     * @throws ShapeException   Exception throw if any parameter is invalid
     * @throws ShapeException   Exception throw if length of any edge is smaller than 0    
     * @throws ShapeException   Exception throw if All vertices are in the same line
     * @throws ShapeException   Exception throw if Sum of two side of triangle is not greater than third side*/
	  
	  public Triangle(Point point1, Point point2, Point point3) throws ShapeException 
	  {

		  if (point1==null || point2==null || point3==null )
			  throw new ShapeException("Invalid Point");
		  
		  	this.point1 = point1;
		  	this.point2 = point2;
		  	this.point3 = point3;
		  
		    line1 = new Line(point1, point2);
		    line2 = new Line(point2, point3);
		    line3 = new Line(point3, point1);
		    
		    a=line1.computeLength();
		    b=line2.computeLength();
		    c=line3.computeLength();


	        if ((point1.getY() - point2.getY()) * (point1.getX() - point3.getX()) == (point1.getY() - point3.getY()) * (point1.getX() - point2.getX()))
	        	throw new ShapeException("All vetices should not be in the same line");
	        
	        if (a + b <= c && b + c <= a && c + a <= b)
	        	throw new ShapeException("Sum of two side of triangle must be greater than third side");
	    }
	  
	    /**
	     * @return  The first vertices
	     */
	    public Point getVertices1() { return point1; }

	    /**
	     * @return  The second vertices
	     */
	    public Point getVertices2() { return point2; }
	    
		  /**
	     * @return  The third vertices
	     */
	    public Point getVertices3() { return point3; }
	    
	    
	    /**
	     * implementation of inherited abstract class 
	     * @param deltaX            The delta x-location by which the rectangle should be moved -- must be a valid double
	     * @param deltaY            The delta y-location by which the rectangle should be moved -- must be a valid double
	     * @throws ShapeException   Exception throw if any parameter is invalid
	     */
	     
	    
	  
	  public void move(double deltaX, double deltaY) throws ShapeException 
		 {
		        point1.move(deltaX, deltaY);
		        point2.move(deltaX, deltaY);
		        point3.move(deltaX, deltaY);
		 }
	  
	  /**
	     * implementation of inherited abstract class 
	     * @return  The area of the Rectangle
	     */

	
	  public double computeArea()
	  {
		  		double s,area;
		  		s = (a+b+c)/2;
		  		area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		  		return area;
	  }


}
