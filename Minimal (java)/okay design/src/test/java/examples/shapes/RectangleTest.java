/**
 * 
 */
package examples.shapes;

import org.junit.Test;

import static org.junit.Assert.*;


/**
 * @author aditi
 *
 */
public class RectangleTest {

	/**
	 * @throws java.lang.Exception
	 */
    @Test
    public void testValidConstruction() throws Exception {
    	Point p1 = new Point(-1.5,3.3);
        Point p2 = new Point(5,3.3);
        Point p3 = new Point(5,1);
        Point p4 = new Point(-1.5,1);

        Rectangle rectangle1= new Rectangle(p1,p2,p3,p4);
        assertEquals(2.30, rectangle1.getHeight(), 0.1);
        assertEquals(6.50, rectangle1.getWidth(), 0.1);


        Rectangle rect= new Rectangle(3.2, 4.2, 8.3, 4.2, 8.3, 1.1, 3.2, 1.1);
        rectangle1 = new Rectangle(3.2, 1.1, 3.2, 4.2, 8.3, 4.2, 8.3, 1.1);
        assertEquals(3.2, rectangle1.getVertices1().getX(), 0);
        assertEquals(1.1, rectangle1.getVertices1().getY(), 0);
        assertEquals(3.2, rectangle1.getVertices2().getX(), 0);
        assertEquals(4.2, rectangle1.getVertices2().getY(), 0);
        assertEquals(8.3, rectangle1.getVertices3().getX(), 0);
        assertEquals(4.2, rectangle1.getVertices3().getY(), 0);
        assertEquals(8.3, rectangle1.getVertices4().getX(), 0);
        assertEquals(1.1, rectangle1.getVertices4().getY(), 0);
        assertEquals(3.2, rectangle1.getLine1().getPoint1().getX(), 0);
        assertEquals(1.1, rectangle1.getLine1().getPoint1().getY(), 0);
        assertEquals(3.2, rectangle1.getLine2().getPoint1().getX(), 0);
        assertEquals(4.2, rectangle1.getLine2().getPoint1().getY(), 0);
        assertEquals(8.3, rectangle1.getLine3().getPoint1().getX(), 0);
        assertEquals(4.2, rectangle1.getLine3().getPoint1().getY(), 0);
        assertEquals(8.3, rectangle1.getLine4().getPoint1().getX(), 0);
        assertEquals(1.1, rectangle1.getLine1().getPoint1().getY(), 0);
        assertEquals(5.10, rectangle1.getHeight(), 0.1);
        assertEquals(3.10, rectangle1.getWidth(), 0.1);

    }

    @Test
    public void testInvalidConstruction() throws ShapeException{
    	
    	Point p1 = new Point(1,1);
        Point p2 = new Point(1,3.5);
        Point p3 = new Point(5.6,3.5);
        Point p4 = new Point(5.6,1);
        
        try {
            new Rectangle(null, p2, p3, p4);
            fail("Expected exception not thrown for when the first parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }

        try {
            new Rectangle(p1, null, p3, p4);
            fail("Expected exception not thrown for when the second parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }

        try {
            new Rectangle(p1, p2, null, p4);
            fail("Expected exception not thrown for when the third parameter is null");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid Point", e.getMessage());
        }


        try {
            new Rectangle(Double.POSITIVE_INFINITY, 1, 1, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Rectangle(1, Double.POSITIVE_INFINITY, 1, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }

        try {
            new Rectangle(1, 1, Double.POSITIVE_INFINITY, 3, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }

        try {
            new Rectangle(1, 1, 1, Double.POSITIVE_INFINITY, 5, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }
        
        try {
            new Rectangle(1, 1, 1, 3,  Double.POSITIVE_INFINITY, 3, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }
        
        try {
            new Rectangle(1, 1, 1, 3, 5, Double.POSITIVE_INFINITY, 5, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }
        
        try {
            new Rectangle(1, 1, 1, 3, 5, 3, Double.POSITIVE_INFINITY, 1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid x-location", e.getMessage());
        }
        
        try {
            new Rectangle(1, 1, 1, 3, 5, 3, 5, Double.POSITIVE_INFINITY);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("Invalid y-location", e.getMessage());
        }

        
        try {
            new Rectangle(p1, p1, p2, p2);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("A line must have a length > 0", e.getMessage());
        }

        try {
            new Rectangle(1, 1, 1, 1, 3, 5, 3, 5);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("A line must have a length > 0", e.getMessage());
        }

        try {
            new Rectangle(1,1,4,3,5,3,6,1);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All angle should be 90 Degree", e.getMessage());
        }

        Point t1= new Point(1,1);
        Point t2= new Point(4,3);
        Point t3= new Point(5,3);
        Point t4= new Point(6,1);


        try {
            new Rectangle(t1, t2, t3, t4);
            fail("Expected exception not thrown");
        } catch (Exception e) {
            assertEquals(ShapeException.class, e.getClass());
            assertEquals("All angle should be 90 Degree", e.getMessage());
        }
    }
    
    @Test
    public void testComputeArea() throws ShapeException {

    	Point p1 = new Point(-1.5,3.3);
        Point p2 = new Point(5,3.3);
        Point p3 = new Point(5,1);
        Point p4 = new Point(-1.5,1);

        Rectangle rectangle1= new Rectangle(p1,p2,p3,p4);
        assertEquals(14.95, rectangle1.computeArea(), 0.001);

        Rectangle rectangle2 = new Rectangle(3.2, 1.1, 3.2, 4.2, 8.3, 4.2, 8.3, 1.1);
        assertEquals(15.81, rectangle2.computeArea(), 0.001);

    }
    
    
    @Test
    public void testMove() throws ShapeException {
        Rectangle myRectangle= new Rectangle(-3, 5, 3, 7, 6, -2, 0, 4);

        myRectangle.move(3, 4);
        assertEquals(0, myRectangle.getVertices1().getX(), 0);
        assertEquals(9, myRectangle.getVertices1().getY(), 0);
        assertEquals(6, myRectangle.getVertices2().getX(), 0);
        assertEquals(11, myRectangle.getVertices2().getY(), 0);
        assertEquals(9, myRectangle.getVertices3().getX(), 0);
        assertEquals(2, myRectangle.getVertices3().getY(), 0);
        assertEquals(3, myRectangle.getVertices4().getX(), 0);
        assertEquals(8, myRectangle.getVertices4().getY(), 0);

        myRectangle.move(.4321, .7654);
        assertEquals(0.4321, myRectangle.getVertices1().getX(), 0);
        assertEquals(9.7654, myRectangle.getVertices1().getY(), 0);
        assertEquals(6.4321, myRectangle.getVertices2().getX(), 0);
        assertEquals(11.7654, myRectangle.getVertices2().getY(), 0);
        assertEquals(9.4321, myRectangle.getVertices3().getX(), 0);
        assertEquals(2.7654, myRectangle.getVertices3().getY(), 0);
        assertEquals(3.4321, myRectangle.getVertices4().getX(), 0);
        assertEquals(8.7654, myRectangle.getVertices4().getY(), 0);


        myRectangle.move(-0.4321, -0.7654);
        assertEquals(0, myRectangle.getVertices1().getX(), 0);
        assertEquals(9, myRectangle.getVertices1().getY(), 0);
        assertEquals(6, myRectangle.getVertices2().getX(), 0);
        assertEquals(11, myRectangle.getVertices2().getY(), 0);
        assertEquals(9, myRectangle.getVertices3().getX(), 0);
        assertEquals(2, myRectangle.getVertices3().getY(), 0);
        assertEquals(3, myRectangle.getVertices4().getX(), 0);
        assertEquals(8, myRectangle.getVertices4().getY(), 0);

    }

}
